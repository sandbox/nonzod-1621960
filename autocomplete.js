/*
* E' inutile ma è buono come promemoria per le modifiche al js di autocompletamento
*/
(function($) {
	$().ready(function() {

		/**
		 * Puts the currently highlighted suggestion into the autocomplete field.
		 */
		Drupal.jsAC.prototype.select = function (node) {
			this.input.value = $(node).data('autocompleteValue');
			//$('#edit-id-profilo').val($(node).data('autocompletePid'));
		};

		/**
		 * Fills the suggestion popup with any matches received.
		 */
		Drupal.jsAC.prototype.found = function (matches) {
			// If no value in the textfield, do not show the popup.
			if (!this.input.value.length) {
				return false;
			}

			// Se non ci sono risultati svuoto il campo id profilo
			if(matches.length == 0) {
				//$('#edit-id-profilo').val('');
			}

			// Prepare matches.
			var ul = $('<ul></ul>');
			var ac = this;
			for (var key in matches) {
				label = matches[key].userdrupal + ':' + matches[key].nome;
				$('<li></li>')
				  .html($('<div></div>').html(label))
				  .mousedown(function () { ac.select(this); })
				  .mouseover(function () { ac.highlight(this); })
				  .mouseout(function () { ac.unhighlight(this); })
				  .data('autocompleteValue', matches[key].userdrupal)
					.data('autocompletePid', matches[key].pid)
				  .appendTo(ul);
			}

			// Show popup with matches, if any.
			if (this.popup) {
				if (ul.children().size()) {
				  $(this.popup).empty().append(ul).show();
				  $(this.ariaLive).html(Drupal.t('Autocomplete popup'));
				}
				else {
				  $(this.popup).css({ visibility: 'hidden' });
				  this.hidePopup();
				}
			}
		};

	});
})(jQuery);
