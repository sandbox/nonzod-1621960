<?php
function user_autocomplete_page($keywords) {
  $matches = array();
  if ($keywords) {
		$query = db_select('users', 'u');
		$query->fields('u', array('name'));
		$a_alias = $query->leftJoin('field_data_field_anagrafica', 'a', 'u.uid = %alias.entity_id');
		$query->addField($a_alias, 'field_anagrafica_first_name', 'first_name');
		$query->addField($a_alias, 'field_anagrafica_last_name', 'last_name');
		$query->condition('u.status', '0', '<>');
		$ored = db_or();
		$ored->condition("{$a_alias}.field_anagrafica_first_name", db_like($keywords) . '%', 'LIKE');
		$ored->condition("{$a_alias}.field_anagrafica_last_name", db_like($keywords) . '%', 'LIKE');
		$query->condition($ored);
		$query->orderBy('u.name');
		$query->range(0, 10);

		$result = $query->execute();
		
   	foreach ($result as $user) {
			$label = $user->name . ': ' .$user->first_name . ' ' . $user->last_name;
      $matches[$user->name] = check_plain($label);
    }
  }

  drupal_json_output($matches);
}
