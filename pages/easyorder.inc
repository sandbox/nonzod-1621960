<?php
/*
 * Page callback
 **/
function ggest_pos_page() {
	$output = drupal_render(drupal_get_form('ggest_pos_orders_form'));

	return $output;
}

/*
 * Implements hook_form()
 **/
function ggest_pos_orders_form($form, &$form_state) {

	if($form_state['values']['cliente']) {
		variable_set('ggest_pos_cliente', $form_state['values']['cliente']);
	}

	$form['dati_cliente'] = array(
  	'#type' => 'fieldset', 
  	'#title' => t('Cliente'), 
		'#weight' => 5, 
		'#collapsible' => TRUE, 
		'#collapsed' => FALSE,
		'#weight' => -1,
	);

	$form['dati_cliente']['cliente'] = array(
  	'#type' => 'textfield', 
  	'#title' => t('Username o nome'), 
  	'#maxlength' => 30, 
		'#size' => 40,
  	'#autocomplete_path' => 'ggest/user-autocomplete',
  	'#default_value' => variable_get('ggest_pos_cliente', ''), 
		'#ajax' => array(
		  'callback' => '_profile_callback',
		  'wrapper' => 'customer-profile',
			//'event' => 'change',
			'method' => 'replace',
      'effect' => 'fade',
		),
  	'#weight' => -1,
	);

	$form['dati_cliente']['id_profilo'] = array(
		'#type' => 'select',
		'#title' => t('Billing address'),
		'#default_value' => $category['selected'],
		'#description' => t('Chose one billing address'),
		'#prefix' => '<div id="customer-profile">',
  	'#suffix' => '</div>'
   );

	$options = array(0 => '-- Chose --');
	// Popolo la select dopo la callback ajax
	if($form_state['values']['cliente']) {
		$user = user_load_by_name($form_state['values']['cliente']);
	}
	if($user) {
		$profili = commerce_customer_profile_load_multiple(array(), array('type' => 'billing', 'uid' => $user->uid, 'status' => TRUE));
		foreach($profili as $pid => $profilo) {
			$p = $profilo->commerce_customer_address[LANGUAGE_NONE][0];
			$options[$pid] = $p['first_name'].' '.$p['last_name'].' - '.$p['thoroughfare'].', '.$p['postal_code'];
		}
	}

	$form['dati_cliente']['id_profilo']['#options'] = $options;

	$form['prodotto'] = array(
  	'#type' => 'fieldset', 
  	'#title' => t('Product'), 
		'#weight' => 5, 
		'#collapsible' => TRUE, 
		'#collapsed' => FALSE,
		'#weight' => 1,
	);

	$form['prodotto']['sku'] = array(
  	'#type' => 'textfield', 
  	'#title' => t('SKU'), 
  	'#maxlength' => 20, 
		'#size' => 40,
  	'#autocomplete_path' => 'commerce_product/autocomplete/commerce_product/line_item_product_selector/product',
  	'#default_value' => '', 
  	'#weight' => 2,
	);

	$form['prodotto']['quantita'] = array(
  	'#type' => 'textfield', 
  	'#title' => t('Quantity'), 
  	'#maxlength' => 4,
		'#size' => 4,
  	'#default_value' => '1', 
  	'#weight' => 3,
	);

	$form['aggiungi_prodotto'] = array(
    '#type' => 'button',
    '#value' => t('Add product'),
    '#ajax' => array(
      'callback' => '_aggiungi_prodotto_callback',
      'wrapper' => 'ajax_prodotti',
      'method' => 'replace',
      'effect' => 'fade',
    ),
		'#weight' => 4,
  );

	if($form_state['values']['sku']) {
		$options = variable_get('ggest_pos_line_items', array());
		$prodotto = commerce_product_load_by_sku($form_state['values']['sku']);
		
		if($prodotto->title) {
			
			$totale = $prodotto->commerce_price['und'][0]['amount'] * $form_state['values']['quantita'];
		
			$options[$form_state['values']['sku']] = array (
				'sku' => $form_state['values']['sku'],
				'prodotto' => $prodotto->title,
				'qta' => $form_state['values']['quantita'],
				'prezzo' => commerce_currency_format($prodotto->commerce_price['und'][0]['amount'],$prodotto->commerce_price['und'][0]['currency_code']),
				'totale' => commerce_currency_format($totale, $prodotto->commerce_price['und'][0]['currency_code']),
				'totale_raw' => $totale,
			);

			variable_set('ggest_pos_line_items', $options);
		}
	}

	// #value is not a good idea
	if($form_state['clicked_button']['#value'] == t('Delete selected')) {
		$options = variable_get('ggest_pos_line_items', array());
		foreach($form_state['values']['line_items'] as $sku => $sel) {
			unset($options[$sel]);
		}

		drupal_set_message(t('Order updated'));
		variable_set('ggest_pos_line_items', $options);
		
	}

	_aggiorna_totale();

	$form['line_items'] = array (
	  '#type' => 'tableselect',
	  '#header' => array (
  		'prodotto' => t('Product'),
  		'qta' => t('Quantity'),
			'prezzo' => t('Price'),
			'totale' => t('Total'),
		),
		'#weight' => 8,
		'#prefix' => '<div id="ajax_prodotti">',
		'#suffix' => '<div id="totale-pos">Totale ordine: <strong>'.variable_get('ggest_totale_pos', '').'</strong></div></div>',
		'#empty' => t('Nessun prodotto inserito'),
	);

	$form['line_items']['#options'] = variable_get('ggest_pos_line_items', array());

	$form['aggiorna_ordine'] = array(
    '#type' => 'button',
    '#value' => t('Delete selected'),
 		'#weight' => 9,
		'#ajax' => array(
      'callback' => '_elimina_prodotto_callback',
      'wrapper' => 'ajax_prodotti',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
	$form['chiudi_ordine'] = array(
    '#type' => 'submit',
    '#value' => t('Close order'),
 		'#weight' => 10,
  );

	return $form;
}

/*
 * Callbacks Ajax
 **/
function _aggiungi_prodotto_callback($form, $form_state) {
	return $form['line_items'];
}

function _profile_callback($form, $form_state) {
	return $form['dati_cliente']['id_profilo'];
}

function _elimina_prodotto_callback($form, $form_state) {
	return $form['line_items'];
}

/*
 * _form_submit()
 **/
function ggest_pos_orders_form_submit($form, &$form_state) {
		$customer = user_load_by_name($form_state['values']['cliente']);
		$order = commerce_order_new($customer->uid, 'completed_gpos');
		commerce_order_save($order);
		$order_wrapper = entity_metadata_wrapper('commerce_order', $order);

		foreach($form_state['values']['line_items'] as $sku => $sel) {
			$product = commerce_product_load_by_sku($sku);
			$line_item = commerce_product_line_item_new($product, 1, $order->order_id);
			commerce_line_item_save($line_item);

			$order_wrapper->commerce_line_items[] = $line_item;
		}

		$order->field_data_consegna = array('und' => 
			array(0 => 
				array(
					'value' => date("Y-m-d H:i:s"),
					'timezone' => 'Europe/Berlin',
					'timezone_db' => 'Europe/Berlin',
					'date_type' => 'datetime'
				)
			)
		);

		// Assegno il profilo cliente (Indirizzo di fatturazione) all'ordine
		$profile_object = array ( 
        'und' => array ( array ( 'profile_id' => $form_state['values']['id_profilo'] , ) , ) , ); 
        
    $order->commerce_customer_billing = $profile_object;

		commerce_order_save($order);

		variable_set('ggest_pos_line_items', '');
		variable_set('ggest_pos_cliente', '');

		drupal_set_message('<a href="/admin/commerce/orders/84'.$order->order_id.'/view">Show order</a>');
}

/*
 * _form_validate()
 * @todo: validazione ordine verifica username, cliente e prodotti
 **/
function ggest_pos_orders_form_validate($form, &$form_state) {

	 if (empty($form_state['#value']['id_profilo']) || $form_state['#value']['id_profilo'] == 0) {
	  form_error($form['dati_cliente']['cliente'], t('Select one Billing address'));
	}

	$user = user_load_by_name($form_state['#value']['cliente']);
  if (empty($user)) {
	  form_error($form['dati_cliente']['cliente'], t('User not found'));
	}
}


/*
 * _form_validate()
 * Validazione del campo SKU, verifico che sia un prodotto valido
 * @delme
 **/
function sku_validate($element, &$form_state, $form) {
	$product = commerce_product_load_by_sku($element['#value']);
  if (empty($product)) {
	  form_error($element, t('Product not found'));
	}
}

/*
 * Aggiorna il totale dell'ordine
 **/
function _aggiorna_totale() {
	$options = variable_get('ggest_pos_line_items', array());

	foreach($options as $item) {
		$totale_pos += $item['totale_raw'];
	}

	variable_set('ggest_totale_pos', commerce_currency_format($totale_pos, $prodotto->commerce_price['und'][0]['currency_code']));
}
